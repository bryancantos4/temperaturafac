package facci.BryanCantos.TemperaturaFaC;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class Convertidor2 extends Activity {

    EditText EditTextconvertir;
    TextView Textviewresultado;
    CheckBox CheckBox1, CheckBox2;
    Button Buttoncampo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertidor2);

        CheckBox1 = (CheckBox) findViewById(R.id.CheckBox1);
        CheckBox2 = (CheckBox) findViewById(R.id.CheckBox2);
        EditTextconvertir = (EditText) findViewById(R.id.EditTextconvertir);
        Buttoncampo = (Button) findViewById(R.id.Buttoncampo);
        Textviewresultado = (TextView) findViewById(R.id.Textviewresultado);

        Buttoncampo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CheckBox1.isChecked ()) {
                    float ingresar = Float.parseFloat(EditTextconvertir.getText().toString());
                    float resultado = (float) ((ingresar * 1.8) + 32);
                    Textviewresultado.setText(String.valueOf(resultado+"°C"));

                }else if(CheckBox2.isChecked ()){
                    float ingresar = Float.parseFloat(EditTextconvertir.getText().toString());
                    float resultado = (float) ((ingresar - 32)/(1.8));
                    Textviewresultado.setText(String.valueOf(resultado+"°C"));
                    }else{
                    Toast.makeText(getApplicationContext(), "Marque un casillero", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}

